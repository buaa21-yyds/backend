from rest_framework import serializers
import question.models


class SettingsSerializer(serializers.ModelSerializer):

    class Meta:
        model = question.models.Settings
        fields = "__all__"


class QuestionnaireSerializer(serializers.ModelSerializer):

    class Meta:
        model = question.models.Questionnaire
        fields = "__all__"


class QuestionSerializer(serializers.ModelSerializer):

    class Meta:
        model = question.models.Question
        fields = "__all__"


class ChoiceSerializer(serializers.ModelSerializer):

    class Meta:
        model = question.models.Choice
        fields = "__all__"
