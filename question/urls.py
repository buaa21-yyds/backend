from django.contrib import admin
from django.urls import path
from question import views

urlpatterns = [
    path('modifySettings', views.modifySettings, name='modifySettings'),
    path('manageQuestionnaire', views.manageQuestionnaire, name='manageQuestionnaire'),
    path('getQuestionnaire', views.getQuestionnaire, name='getQuestionnaire'),
    path('answerQuestionnaire', views.answerQuestionnaire, name='answerQuestionnaire'),
    path('createQuestionnaire', views.createQuestionnaire, name='createQuestionnaire'),
    path('modifyQuestionnaire', views.modifyQuestionnaire, name='modifyQuestionnaire'),
    path('deleteQuestionnaire', views.deleteQuestionnaire, name='deleteQuestionnaire'),
    path('releaseQuestionnaire', views.releaseQuestionnaire, name='releaseQuestionnaire'),
    path('starQuestionnaire', views.starQuestionnaire, name='starQuestionnaire'),
    path('removeQuestionnaire', views.removeQuestionnaire, name='removeQuestionnaire'),
    path('copyQuestionnaire', views.copyQuestionnaire, name='copyQuestionnaire'),
    path('exportQuestionnaire', views.exportQuestionnaire, name='exportQuestionnaire'),
    path('createQuestion', views.createQuestion, name='createQuestion'),
    path('modifyQuestion', views.modifyQuestion, name='modifyQuestion'),
    path('deleteQuestion', views.deleteQuestion, name='deleteQuestion'),
    path('questionnaireID', views.questionnaireID, name='questionnaireID'),
    path('questionID', views.questionID, name='questionID'),
    path('choiceID', views.choiceID, name='choiceID'),
]
