from Crypto.Cipher import AES
import base64

class prpcrypt(object):
    def __init__(self, key=')_9-+klo@c4t$k$w'):
        self.key = key.encode("UTF-8")
        self.mode = AES.MODE_CBC

    # cryptographic functions
    def encrypt(self, text):
        cryptor = AES.new(self.key, self.mode, self.key)
        length = 16
        count = len(text)
        add = length - (count % length)
        text = text + ('\0'*add)
        text = text.encode("UTF-8")
        self.ciphertext = cryptor.encrypt(text)
        return base64.b16encode(self.ciphertext).decode()

    def decrypt(self, text):
        text = text.encode("UTF-8")
        cryptor = AES.new(self.key, self.mode, self.key)
        plain_text = cryptor.decrypt(base64.b16decode(text))
        return plain_text.rstrip(b'\0').decode()


encoder = prpcrypt()
print(encoder.encrypt(str(20)))
