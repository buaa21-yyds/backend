from django.contrib import admin
from question.models import *

admin.site.register(Questionnaire)
admin.site.register(Question)
admin.site.register(Choice)
admin.site.register(Settings)
