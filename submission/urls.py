from django.urls import path
from submission import views

urlpatterns = [
    path('crtsub', views.SubmissionViews.createSubmission),
    path('delsub', views.SubmissionViews.deleteSubmission),
    path('getsub', views.SubmissionViews.getSubmissionAnswers),
    path('allsub', views.SubmissionViews.getAllSubmissions),
    path('savans', views.SubmissionViews.saveAnswer),
    path('submit', views.SubmissionViews.submitQuestionnaire),
    path('report', views.ReportViews.exportExcel),
    path('stats', views.ReportViews.getStatistic),
    path('qesrep', views.ReportViews.getStatByQuestion),
    path('getremain', views.EnrollViews.getRemain),
    path('abc', views.ReportViews.getChoiceAB),
    path('getscore', views.ReportViews.getScore),
    # path('crtexamsub', views.ExamViews.createExamSubmission),
]