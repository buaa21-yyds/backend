from django.db import models
from question.models import Question, Questionnaire, Choice
from django.contrib.auth.models import User


# Create your models here.

# 用户提交记录
class Submission(models.Model):
    submitUID = models.CharField(max_length=256, blank=False, null=False)  # 用户标识符，前端传来的
    createTime = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    submitTime = models.DateTimeField(auto_now=True, verbose_name="提交时间")
    questionnaireTo = models.ForeignKey(Questionnaire, verbose_name="所属问卷", on_delete=models.CASCADE, null=False)
    isSubmitted = models.BooleanField(default=False, verbose_name="是否已提交")
    IPaddress = models.GenericIPAddressField(blank=True, null=True)
    grade = models.IntegerField(blank=True, null=True)


# 题目作答记录
class Answer(models.Model):
    gradeStatusChoices = [(0, '错误'), (1, '部分正确'), (2, '完全正确'), (3, '自行评判')]
    submissionTo = models.ForeignKey(Submission, verbose_name="所属提交记录", on_delete=models.CASCADE)
    questionTo = models.ForeignKey(Question, verbose_name="对应问题", blank=False, null=False, on_delete=models.CASCADE)
    answerChoices = models.ManyToManyField(Choice)
    answerText = models.TextField(max_length=1024, verbose_name="填空题内容", blank=True, null=True)  # 若题目类型为填空题则直接对应该文本域内容
    answerScore = models.IntegerField(blank=True, null=True)
    gradeStatus = models.IntegerField(choices=gradeStatusChoices, blank=True, null=True)
