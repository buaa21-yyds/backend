from django.contrib import admin
from submission.models import *

# Register your models here.
admin.site.register(Submission)
admin.site.register(Answer)