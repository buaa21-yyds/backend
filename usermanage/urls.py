from django.urls import path
from . import views

urlpatterns = [
    path('login/', views.UserViews.userLogin),
    path('logout/', views.UserViews.userLogout),
    path('register/', views.UserViews.userRegister),
    path('changeinfo/', views.UserViews.userChangeInfo),
    path('resetpassword/', views.UserViews.userResetPassword),
    path('getinfo/', views.UserViews.userGetInfo),
    path('importxls/', views.UserViews.importUser),
]
